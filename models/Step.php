<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblstep".
 *
 * @property int $id
 * @property int $queue_id
 * @property string $name
 *
 * @property Tblqueue $queue
 * @property Tblinfo[] $tblinfos
 */
class Step extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tblstep';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['queue_id', 'name'], 'required'],
            [['queue_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            //[['queue_id'], 'exist', 'skipOnError' => true, 'targetClass' => Queue::className(), 'targetAttribute' => ['queue_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'queue_id' => 'Queue ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Queue]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQueue()
    {
        return $this->hasOne(Queue::className(), ['id' => 'queue_id']);
    }

    /**
     * Gets query for [[Tblinfos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTblinfos()
    {
        return $this->hasMany(Info::className(), ['step_id' => 'id']);
    }
}
