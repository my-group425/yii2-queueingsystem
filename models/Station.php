<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblstation".
 *
 * @property int $id
 * @property int $queue_id
 * @property int|null $step_id
 * @property string $name
 *
 * @property Tblqueue $queue
 * @property Tblstep $step
 */
class Station extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $stationCustomers = [];
    public static function tableName()
    {
        return 'tblstation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['queue_id', 'name'], 'required'],
            [['queue_id', 'step_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['queue_id'], 'exist', 'skipOnError' => true, 'targetClass' => Queue::className(), 'targetAttribute' => ['queue_id' => 'id']],
            [['step_id'], 'exist', 'skipOnError' => true, 'targetClass' => Step::className(), 'targetAttribute' => ['step_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'queue_id' => 'Queue',
            'step_id' => 'Step',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Queue]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQueue()
    {
        return $this->hasOne(Queue::className(), ['id' => 'queue_id']);
    }

    /**
     * Gets query for [[Step]].
     *
     * @return \yii\db\ActiveQuery
     */

    public function getStep()
    {
        return $this->hasOne(Step::className(), ['id' => 'step_id']);
    }

    public function getAddcustomers($customer)
    {
        $this->stationCustomers->push($customer);
    }
}
