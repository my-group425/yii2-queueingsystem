<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblqueue".
 *
 * @property int $id
 * @property int $status
 *
 * @property Tblstep[] $tblsteps
 */
class Queue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tblqueue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Queue',
            'status' => 'Status',
            'name' => 'Queue Name',
        ];
    }

    /**
     * Gets query for [[Tblsteps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTblsteps()
    {
        return $this->hasMany(Step::className(), ['queue_id' => 'id']);
    }

    public function getSteps($queue){
        $step = Step::find()
        ->where(['queue_id' => $queue])
        ->count();
        $queue = Queue::find()
          ->where(['status' => 2])
          ->orderBy(['id' => SORT_DESC])
          ->count();

        $result = ($step > 0 && $queue == 0) ?  [1 => 'Draft', 2 => 'Posted'] : [1 => 'Draft'];
        return $result;
    }
}
