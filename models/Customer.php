<?php

namespace app\models;

use Yii;
use app\models\Station;

/**
 * This is the model class for table "tblcustomer".
 *
 * @property int $id
 * @property int $queue_id
 * @property int $step_id
 * @property int $status
 * @property string $date_created
 *
 * @property Tblqueue $queue
 * @property Tblstep $step
 * @property TblcustomerInfo[] $tblcustomerInfos
 */
class Customer extends \yii\db\ActiveRecord
{

    public $cid;
    public $qid;

    public $stationCustomers = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tblcustomer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['queue_id', 'step_id', 'status'], 'required'],
            [['queue_id', 'step_id', 'status'], 'integer'],
            [['date_created'], 'safe'],
            [['queue_id'], 'exist', 'skipOnError' => true, 'targetClass' => Queue::className(), 'targetAttribute' => ['queue_id' => 'id']],
            [['step_id'], 'exist', 'skipOnError' => true, 'targetClass' => Step::className(), 'targetAttribute' => ['step_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'queue_id' => 'Queue ID',
            'step_id' => 'Step ID',
            'status' => 'Status',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * Gets query for [[Queue]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQueuez()
    {
        return $this->hasOne(Queue::className(), ['id' => 'queue_id']);
    }

    /**
     * Gets query for [[Step]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStep()
    {
        return $this->hasOne(Step::className(), ['id' => 'step_id']);
    }

    /**
     * Gets query for [[TblcustomerInfos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTblcustomerInfos()
    {
        return $this->hasMany(CustomerInfo::className(), ['customer_id' => 'id']);
    }


    //assign queue number
    public function getQueueNumber($queue)
    {
        $datefrom = date("Y-m-d");
        $queue =  Customer::find()
        ->where(['DATE(date_created)' => $datefrom])
        ->andWhere(['queue_id' => $queue])
        ->count();
        return $queue + 1;


    }
    //get all customer for today
    public function getAllCustomers()
    {
        $datefrom = date("Y-m-d");
        return Customer::find()
        ->select('tblcustomer.id as cid, tblcustomer.queue as qid')
        ->leftJoin('tblqueue', 'tblqueue.id = tblcustomer.queue_id')
        ->where(['DATE(date_created)' => $datefrom])
        ->andWhere(['tblqueue.status' => 2])
        ->andWhere(['tblcustomer.status' => 1])
        ->all();
        
    }

    //display first customer in queue
    public function subCustomer()
    {
        $customers = $this->getAllCustomers();
        
        foreach ($customers as $key => $value) {
            array_push($this->stationCustomers, $value->cid);
           //array_push($queue, $ci);
        }
        $i = array_shift($this->stationCustomers);
        return $i;
    }

    public function getCustomer($id)
    {
        return $this->find()
        ->where(['id' => $id])
        ->one();
    }

}
