<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblstation".
 *
 * @property int $id
 * @property int $queue_id
 * @property int|null $step_id
 * @property string $name
 *
 * @property Tblqueue $queue
 * @property Tblstep $step
 */
class Stations 
{
    /**
     * {@inheritdoc}
     */

   
   
    public function __construct($stationName) {
        $this->stationName = $stationName;
    }

   
   
}
