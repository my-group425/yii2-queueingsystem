<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblcustomer_info".
 *
 * @property int $customer_id
 * @property int $info_id
 * @property string $value
 *
 * @property Tblcustomer $customer
 * @property Tblinfo $info
 */
class CustomerInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tblcustomer_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'info_id', 'value'], 'required'],
            [['customer_id', 'info_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
           // [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['info_id'], 'exist', 'skipOnError' => true, 'targetClass' => Info::className(), 'targetAttribute' => ['info_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'info_id' => 'Info ID',
            'value' => 'Value',
        ];
    }

    /**
     * Gets query for [[Customer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Tblcustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * Gets query for [[Info]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInfo()
    {
        return $this->hasOne(Tblinfo::className(), ['id' => 'info_id']);
    }
}
