<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tblinfo".
 *
 * @property int $id
 * @property int $step_id
 * @property string $title
 * @property string $type
 *
 * @property Tblstep $step
 * @property TblcustomerInfo[] $tblcustomerInfos
 */
class Info extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tblinfo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'type'], 'safe'],
            [['step_id'], 'integer'],
            [['title', 'type'], 'string', 'max' => 255],
         //   [['step_id'], 'exist', 'skipOnError' => true, 'targetClass' => Step::className(), 'targetAttribute' => ['step_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'step_id' => 'Step ID',
            'title' => 'Title',
            'type' => 'Type',
        ];
    }

    /**
     * Gets query for [[Step]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStep()
    {
        return $this->hasOne(Step::className(), ['id' => 'step_id']);
    }

    /**
     * Gets query for [[TblcustomerInfos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTblcustomerInfos()
    {
        return $this->hasMany(CustomerInfo::className(), ['info_id' => 'id']);
    }
}
