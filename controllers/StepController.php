<?php

namespace app\controllers;
use Yii;
use app\models\Info;
use app\models\InfoSearch;
use yii\helpers\ArrayHelper;
use app\models\Model;
use app\models\Step;
use app\models\StepSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * StepController implements the CRUD actions for Step model.
 */
class StepController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return 
        array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                   // 'only' => ['logout'],
                    'rules' => [
                        [
                            'actions' => ['create', 'index', 'update', 'view', 'delete'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                        [
                            'actions' => ['login'],
                            'allow' => true,
                            // 'roles' => ['?'],
                        ],
                        
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Step models.
     *
     * @return string
     */
    public function actionIndex($queue)
    {
        $searchModel = new StepSearch();
        $dataProvider = $searchModel->search($queue, $this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'queue' => $queue,
        ]);
    }

    /**
     * Displays a single Step model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchInfo = new InfoSearch();
        $dataProvider = $searchInfo->search($id, $this->request->queryParams);


        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchInfo' => $searchInfo,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Step model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($queue)
    {
        $model = new Step();
        $modelsinfo = [new Info];
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) ) {

                $modelsinfo = Model::createMultiple(Info::classname());
                Model::loadMultiple($modelsinfo, Yii::$app->request->post());

                // ajax validation
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ArrayHelper::merge(
                        ActiveForm::validateMultiple($modelsinfo),
                        ActiveForm::validate($model)
                    );
                }

                // validate all models
                $valid = $model->validate();
                $valid = Model::validateMultiple($modelsinfo) && $valid;
                
                if ($valid) {
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $model->save(false)) {
                          
                            foreach ($modelsinfo as $key => $modelinfo) {
                                $modelinfo->step_id = $model->id;
                                
                                if (! ($flag = $modelinfo->save(false))) {

                                    $transaction->rollBack();
                                    break;

                                }
                                //error_log($transaction);
                            }
                        }
                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['index', 'queue' => $queue]);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }

            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'queue' => $queue,
            'modelsinfo' => (empty($modelsinfo)) ? [new Info] : $modelsinfo,
        ]);
    }

    /**
     * Updates an existing Step model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $modelsinfo = $this->findInfo($id);

        if ($model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($modelsinfo, 'id', 'id');
            $modelsinfo = Model::createMultiple(Info::classname(), $modelsinfo);
            Model::loadMultiple($modelsinfo, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsinfo, 'id', 'id')));

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsinfo) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (!empty($deletedIDs)) {
                            Address::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsinfo as $modelinfo) {
                            $modelinfo->step_id = $model->id;
                            if (! ($flag = $modelinfo->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        
     

        return $this->render('update', [
            'model' => $model,
            'modelsinfo' => (empty($modelsinfo)) ? [new Info] : $modelsinfo
        ]);
    }

    /**
     * Deletes an existing Step model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Step model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Step the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Step::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function findInfo($id)
    {
      $model = Info::find()->where(['step_id' => $id])->all();
      return $model;
    }
}
