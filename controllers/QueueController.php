<?php

namespace app\controllers;
use Yii;
use app\models\Info;
use app\models\InfoSearch;
use app\models\Model;
use app\models\Queue;
use app\models\QueueSearch;
use app\models\Step;
use app\models\StepSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * QueueController implements the CRUD actions for Queue model.
 */
class QueueController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                   // 'only' => ['logout'],
                    'rules' => [
                        [
                            'actions' => ['create', 'index', 'update', 'view', 'delete'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                        [
                            'actions' => ['login'],
                            'allow' => true,
                            // 'roles' => ['?'],
                        ],
                        
                    ],
                ],

                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Queue models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new QueueSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Queue model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchStep = new StepSearch();
        $dataProvider = $searchStep->search($id, $this->request->queryParams);
        $step = new Step();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'step' => $this->findStep($id),
            'searchStep' => $searchStep,
            'dataProvider' => $dataProvider,
            'step' => $step,
        ]);
    }

    /**
     * Creates a new Queue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Queue();
        $step = [new Step];
        $info = [new Info];

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) ) {

                $step = Model::createMultiple(Step::classname());
                Model::loadMultiple($step, Yii::$app->request->post());

                $info = Model::createMultiple(Info::classname());
                Model::loadMultiple($info, Yii::$app->request->post());

                // ajax validation
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ArrayHelper::merge(
                        ActiveForm::validateMultiple($info),
                        ActiveForm::validateMultiple($step),
                        ActiveForm::validate($model)
                    );
                }

                // validate all models
                $valid = $model->validate();
                $valid = Model::validateMultiple($step) && $valid;
                $valid = Model::validateMultiple($info) && $valid;
                if ($valid) {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        if ($flag = $model->save(false)) {
                          
                            foreach ($step as $key => $stepz) {
                                $stepz->queue_id = $model->id;

                                
                                
                                if (! ($flag = $stepz->save(false))) {

                                    $transaction->rollBack();
                                    break;

                                }

                                foreach ($info as $key => $infoz) {
                                    $infoz->step_id = $stepz->id;
                                
                                    if (! ($flag = $infoz->save(false))) {
                                        $transaction->rollBack();
                                        break;
                                    }
                                }
                                //error_log($transaction);
                            }
                        }
                        if ($flag) {
                            $transaction->commit();
                            return $this->redirect(['step/index', 'queue' => $model->id]);
                        }
                    } catch (Exception $e) {
                        $transaction->rollBack();
                    }
                }

            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'info' => (empty($info)) ? [new Info] : $info,           
            'step' => (empty($step)) ? [new Step] : $step,
        ]);
    }

    /**
     * Updates an existing Queue model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $queue = new Queue();
       
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Queue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Queue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Queue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Queue::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

     protected function findStep($id)
    {
        if (($model = Step::findAll(['queue_id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }    
}
