<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index ">
<?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'queue_id')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'step_id')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'date_created')->hiddenInput()->label(false) ?>

        <div class="form-group">
        <?= Html::submitButton('NEXT', ['class' => 'btn btn-primary btn-lg']) ?>    
        <?= Html::a('HOLD', ['hold', 'id' => $model->id], ['class' => 'btn btn-primary btn-lg']) ?>
        <?= Html::a('NO SHOW', ['#'], ['class' => 'btn btn-primary btn-lg']) ?>
  
    <br><br>
    <div class="row">
        <div class="col-sm-6">
        <div class="text-center box box-primary">
            <div class=" box-heading">
                <h4>Active Queue number</h4>

            </div>
            <div class=" box-body">
                <h1><?= $model->queue; ?></h1>
            <p>ID No: <?= $model->id ?></p>
           
            </div>
        </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<style>
    .btn-lg{
        padding: 50px;
        width: 200px;
    }
</style>

