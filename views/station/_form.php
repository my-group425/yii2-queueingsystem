<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Arrayhelper;
/* @var $this yii\web\View */
/* @var $model app\models\Station */
/* @var $form yii\widgets\ActiveForm */

$typelist = \yii\helpers\Url::to(['station/step']); 

?>

<div class="station-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'queue_id')->widget(Select2::classname(), [
	    'data' => ArrayHelper::map($queue->find()->where(['status' => 2])->all(),'id','name'),
        'pluginOptions' => [
                'id' => 'queue',
                'name' => 'queue',
        ],
        'pluginEvents'=>[
            'select2:select'=>'function(){
                var vals = this.value;

                 $.ajax({
                    url: "'.$typelist.'",
                    data: {"queue":vals},    
                    type: "post",                         
                    success: function (result) {
                        console.log(result);
                        $(".step-select").html("").select2({ data:result, theme:"krajee", width:"100%",placeholder:"Select Step",
                            allowClear: false});
                        $(".step-select").select2("val","");
                    },
                    error: function (result) {
                        console.log(result);
                    }
                });


            }'
        ]	    
	]); ?>  

    <?= $form->field($model, 'step_id')->widget(Select2::classname(), [
        'data' => '',
        'options' => [
               // 'id' => 'step',
                'class' => 'step-select',
        ],
        //'data' => ArrayHelper::map($queue->find()->where(['status' => 2])->all(),'id','name'),
         
    ]); ?>  
    
    
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
