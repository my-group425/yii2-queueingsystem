<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
?>
<aside class="main-sidebar">

    <section class="sidebar">
       
       
        <!-- Sidebar user panel -->
        

        <!-- search form -->
       <!--  <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'User Management', 'icon' => 'user', 'url' => ['/user/admin']],
                    ['label' => 'Queue Management', 'icon' => 'dashboard', 'url' => ['/queue']],
                    ['label' => 'Customer Management', 'icon' => 'group', 'url' => ['/customer']],
                    ['label' => 'Settings', 'icon' => 'gear', 'url' => ['/station']],
                    //['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                   
                ],
            ]
        ) ?>
        

        <div class="bot">
            <div class="collapse " id="collapseExample">
                <div class="user-panel pull-left"> 
                    <div class=" m-3 ">

                         <?= Html::a(
                            '<i class="fa fa-cogs fa-fw"></i> Profile',
                            ['/user/settings'],
                            ['class' => ' ']
                        ) ?><br>
                    </div>
                    <div class=" m-3">
                        <?= Html::a(
                            '<i class="fa fa-sign-out fa-fw"></i> Log out',
                            ['/user/logout'],
                            ['data-method' => 'post', 'class' => '', 'id' => 'btn_logout']
                        ) ?>
                       
                    </div>
                </div>
            </div>
        
            <a class="" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle"alt="User Image"/>
                        
                    </div>
                    <div class="pull-left info">
                        <p><?= Yii::$app->user->identity->username ?> <i class="pull-right fa fa-ellipsis-h"></i></p>
                        <small><?= Yii::$app->user->identity->email ?></small>
                    </div>
                    <div class=" m-3">                        
                        
                    </div>

                </div>
            </a>
        </div>


    </section>

</aside>
