<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header ">
<!-- <img src="'.Url::base(true).'/img/icad_logo.png" class="user-image"/>
<img src="'.Url::base(true).'/img/icad_logo.png" class="user-image"/> -->
    <?= Html::a('<span class="logo-mini">P</span><span class="logo-lg"><b></b> </span> ', Yii::$app->homeUrl, ['class' => 'logo hvr-pulse-grow']) ?>

    <input type="hidden" id="current_action" value="<?= (Yii::$app->controller->action->id === null)? '':Yii::$app->controller->action->id?>">
    <input type="hidden" id="current_controller" value="<?= (Yii::$app->controller->id === null)? '':Yii::$app->controller->id?>">
 <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
   
</header>
