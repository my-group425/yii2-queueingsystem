<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Queue */
/* @var $form yii\widgets\ActiveForm */
?>
<br>
<div class="queue-form">
 <h3>Create queue </h3>
    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <div hidden>

     <?php echo $form->field($model, 'status')->textInput(['value' => 1]) ?>
    </div>
    <?php echo $form->field($model, 'name')->textInput([]) ?>
    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
        'widgetBody' => '.container-items', // required: css class selector
        'widgetItem' => '.item', // required: css class
        'limit' => 10, // the maximum times, an element can be cloned (default 999)
        'min' => 1, // 0 or 1 (default 1)
        'insertButton' => '.add-item', // css class
        'deleteButton' => '.remove-item', // css class
        'model' => $step[0],
        'formId' => 'dynamic-form',
        'formFields' => [
            'name',
        
        ],
    ]); ?>
    <div class="panel panel-success">
        <div class="panel-heading">
            <i class="fa fa-info"></i> &nbsp; Categories
            <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i></button>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body container-items"><!-- widgetContainer -->
            <?php foreach ($step as $index => $stepz): ?>
                <div class="item">
                    <?php
                        // necessary for update action.
                        if (!$stepz->isNewRecord) {
                            echo Html::activeHiddenInput($stepz, "[{$index}]id");
                        }
                    ?>
                   
                    <div class="row">
                         <div hidden>
                             <?= $form->field($stepz, "[{$index}]queue_id")->textInput(['maxlength' => true, 'placeholder' => 'Field', 'value' => Yii::$app->db->getLastInsertID()+1])->label(false); ?>
                        </div>
                        <div class="col-sm-10">
                             <?= $form->field($stepz, "[{$index}]name")->textInput(['maxlength' => true, 'placeholder' => 'Field'])->label(false); ?>
                        </div>
                        
                        <div class="col-sm-2 ">
                            <button type="button" class="pull-left remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                        <?php DynamicFormWidget::begin([
                            'widgetContainer' => 'dynamicform_wrapper_info',
                            'widgetBody' => '.container-info', // required: css class selector
                            'widgetItem' => '.info', // required: css class
                            'limit' => 10, // the maximum times, an element can be cloned (default 999)
                            'min' => 0, // 0 or 1 (default 1)
                            'insertButton' => '.add-info', // css class
                            'deleteButton' => '.remove-info', // css class
                            'model' => $info[0],
                            'formId' => 'dynamic-form',
                            'formFields' => [
                                'title',
                                'type',
                            
                            ],
                        ]); ?>
                        <div class="panel panel-default ">
                            <div class="panel-heading">
                                <i class="fa fa-info"></i> &nbsp; Add Information
                                <button type="button" class="pull-right add-info btn btn-success btn-xs"><i class="fa fa-plus"></i></button>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body container-info"><!-- widgetContainer -->
                                <?php foreach ($info as $index => $infoz): ?>
                                    <div class="info">
                                        <?php
                                            // necessary for update action.
                                            if (!$infoz->isNewRecord) {
                                                echo Html::activeHiddenInput($infoz, "[{$index}]id");
                                            }
                                        ?>
                                       
                                        <div class="row">
                                             <div >
                                                <?php  $form->field($infoz, "[{$index}]step_id")->textInput(['maxlength' => true, 'placeholder' => 'Field', 'value' => Yii::$app->db->getLastInsertID()+1])->label(false); ?>
                                            </div>
                                            <div class="col-sm-5">
                                                 <?= $form->field($infoz, "[{$index}]title")->textInput(['maxlength' => true, 'placeholder' => 'Title'])->label(false); ?>
                                            </div>
                                            <div class="col-sm-5">

                                                <?= $form->field($infoz, "[{$index}]type")->dropDownList(['Input' => 'Input', 'Number' => 'Number', 'Boolean' => 'Boolean'], ['maxlength' => true, 'placeholder' => 'type'])->label(false); ?>
                                                
                                            </div>
                                            
                                            <div class="col-sm-2">
                                                <button type="button" class="pull-right remove-info btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                                                <div class="clearfix"></div>
                                            </div>
                                            
                                        </div>

                                            
                                        
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php DynamicFormWidget::end(); ?>
                    

                        
                    
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php DynamicFormWidget::end(); ?>
   
    <p>You will be redirected to the list of categories to edit the information needed for each.</p>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    	
    </div>

    <?php ActiveForm::end(); ?>

</div>
