<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Queue */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Queues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="queue-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'label' => 'Status',
                'value' => function($model){
                    if ($model->status == 1){
                        return 'Draft';
                    }
                    else if ($model->status == 2){
                        return 'Posted';
                    }
                }
            ],
        ],
    ]) ?>
         
    <div class="panel panel-default">
        <div class="panel-heading">Categories</div>
        <div class="panel-body">
            <?= Html::a('Create categories', ['/step/create', 'queue' => $model->id], ['class' => 'btn btn-success']) ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchStep,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    'name',
                    [
                        'class' => ActionColumn::className(),
                        'urlCreator' => function ($action, app\models\Step $step, $key, $index, $column) {
                            return Url::toRoute(['/step/' .$action, 'id' => $step->id]);
                         }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>


<style>
.panel > .panel-heading {
/*    background-image: none;
    background-color: #00a65a;
    color: white;
*/
}
</style>
