<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Queue */
/* @var $form yii\widgets\ActiveForm */
?>
<br><br>
<div class="queue-form">

	<h3>Queue</h3>
   <?php $form = ActiveForm::begin(); ?>
   <div class="col-sm-6">
<?php echo $form->field($model, 'name')->textInput([]) ?>	
   </div>
   <div class="col-sm-6">
    <?php  
    $data = [1 => 'Draft', 2 => 'Posted']; ?>
    <?= $form->field($model, "status")->dropDownList($model->getSteps($model->id), ['class' => 'form-control formula_input','prompt' => 'select status']); ?>
   </div>
      
  
    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
