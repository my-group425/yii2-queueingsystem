<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel app\models\StepSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Steps';
$this->params['breadcrumbs'][] = $this->title;
?><br>
<div class="step-index">

    
    
    <?= Html::a('<i class="fa fa-plus"></i> Add Category', ['create', 'queue' => $queue], ['class' => 'btn btn-success btn-flat']) ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'queue_id',
            'name',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, app\models\Step $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>

<?php 
    Modal::begin([
        'header' => '<h4>Points</h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";


$script = <<< JS
    
    $(document).on('click','#modalButton', function(e){
    
        $('#modal').modal('show')
        .find('#modalContent')
        .load($(this).attr('value'));
    });
    
JS;
$this->registerJs($script);

 ?>
