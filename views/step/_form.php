<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Step */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="step-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div hidden>
    <?php echo $form->field($model, 'queue_id')->textInput(['value' => $queue]) ?>
    </div>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
        'widgetBody' => '.container-items', // required: css class selector
        'widgetItem' => '.item', // required: css class
        'limit' => 10, // the maximum times, an element can be cloned (default 999)
        'min' => 0, // 0 or 1 (default 1)
        'insertButton' => '.add-item', // css class
        'deleteButton' => '.remove-item', // css class
        'model' => $modelsinfo[0],
        'formId' => 'dynamic-form',
        'formFields' => [
            'title',
            'type',
            
        ],
    ]); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-info"></i> &nbsp;Add Information
            <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i></button>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body container-items"><!-- widgetContainer -->
            <?php foreach ($modelsinfo as $index => $modelinfo): ?>
                <div class="item">
                    <?php
                        // necessary for update action.
                        if (!$modelinfo->isNewRecord) {
                            echo Html::activeHiddenInput($modelinfo, "[{$index}]id");
                        }
                    ?>
                   
                    <div class="row">
                    	
                        <div class="col-sm-5">
                        	 <?= $form->field($modelinfo, "[{$index}]title")->textInput(['maxlength' => true, 'placeholder' => 'Field'])->label(false); ?>
                        </div>
                        <div class="col-sm-5">
                            <?= $form->field($modelinfo, "[{$index}]type")->textInput(['maxlength' => true, 'placeholder' => 'Type'])->label(false); ?>
                        </div>
                        <div class="col-sm-1">
                        	<button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                    		<div class="clearfix"></div>
                        </div>
                    </div>

                        
                    
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php DynamicFormWidget::end(); ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
