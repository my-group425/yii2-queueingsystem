<?php

use yii\db\Migration;

class m220601_080851_create_table_tblcustomer extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tblcustomer}}', [
            'id' => $this->primaryKey(),
            'queue_number' => $this->string(11)->notNull(),
            'status' => $this->integer()->notNull(),
            'date_created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%tblcustomer}}');
    }
}
