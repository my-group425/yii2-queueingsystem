<?php

use yii\db\Migration;

class m220601_080901_create_table_tblcustomer_info extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tblcustomer_info}}', [
            'customer_id' => $this->integer()->notNull(),
            'info_id' => $this->integer()->notNull(),
            'value' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('customer_info', '{{%tblcustomer_info}}', 'customer_id', '{{%tblcustomer}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('info_customer', '{{%tblcustomer_info}}', 'info_id', '{{%tblinfo}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%tblcustomer_info}}');
    }
}
