<?php

use yii\db\Migration;

class m220704_013417_create_table_tblstation extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tblstation}}', [
            'id' => $this->primaryKey(),
            'queue_id' => $this->integer()->notNull(),
            'step_id' => $this->integer(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('station_queue', '{{%tblstation}}', 'queue_id', '{{%tblqueue}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('station_step', '{{%tblstation}}', 'step_id', '{{%tblstep}}', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%tblstation}}');
    }
}
