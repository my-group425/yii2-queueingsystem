<?php

use yii\db\Migration;

class m220704_013359_create_table_tblcustomer extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tblcustomer}}', [
            'id' => $this->primaryKey(),
            'queue_id' => $this->integer()->notNull(),
            'step_id' => $this->integer()->notNull(),
            'queue' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'date_created' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey('customer_queue', '{{%tblcustomer}}', 'queue_id', '{{%tblqueue}}', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('customer_step', '{{%tblcustomer}}', 'step_id', '{{%tblstep}}', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%tblcustomer}}');
    }
}
