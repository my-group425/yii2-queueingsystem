<?php

use yii\db\Migration;

class m220601_080831_create_table_tblinfo extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tblinfo}}', [
            'id' => $this->primaryKey(),
            'step_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('step_info', '{{%tblinfo}}', 'step_id', '{{%tblstep}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%tblinfo}}');
    }
}
