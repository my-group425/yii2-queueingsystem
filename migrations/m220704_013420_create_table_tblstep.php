<?php

use yii\db\Migration;

class m220704_013420_create_table_tblstep extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tblstep}}', [
            'id' => $this->primaryKey(),
            'queue_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('queue_step', '{{%tblstep}}', 'queue_id');
    }

    public function down()
    {
        $this->dropTable('{{%tblstep}}');
    }
}
