<?php

namespace app\modules\queueing;

/**
 * Queueing module definition class
 */
class Queueing extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\queueing\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->layout = '@app/modules/queueing/views/layouts/main';
        // custom initialization code goes here
    }
}
