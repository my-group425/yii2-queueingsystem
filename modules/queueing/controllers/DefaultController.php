<?php

namespace app\modules\queueing\controllers;
use Yii;
use yii\web\Controller;
use app\models\Queue;
use app\models\Station;
use app\models\Step;
use app\models\Customer;
use app\models\CustomerInfo;

use app\models\Info;
/**
 * Default controller for the `Queueing` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {	
    	$queue = new Queue();
    	$step = new Step();
    	$info = new Info();
        $model = new Customer();

    	$posted = $queue->find()
    	->where(['status' => 2])
    	->orderBy(['id' => SORT_DESC])
    	->one();

    	$steps = $step->find()
    	->where(['queue_id' => $posted['id']])
    	->all();

        //$numberfortoday = $model->find()->where(['between', 'date_created', $datefrom, $dateto])->all();
       
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->queue = $model->getQueueNumber($posted['id']);

                if ($model->save()) {
                    return $this->redirect(['queue', 'selected' => $model->step_id, 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }


        return $this->render('index', [
        	'queue' => $queue,
        	'step' => $step,
        	'info' => $info,
        	'posted' => $posted,
        	'steps' => $steps,
            'model' => $model,
           // 'numberfortoday' => $numberfortoday,
        ]);
    }

    public function actionInfo($selected)
    {   
        $queue = new Queue();
        $step = new Step();
        $info = new Info();
        $model = new CustomerInfo();
        $customer = new Customer();
        $station = new Station();
        $infos = $info->find()
        ->where(['step_id' => $selected])
        ->all();

        $steps = $step->find()->where(['id' => $selected])->one();

        $today = date("Y-m-d");
        $numberfortoday = $customer->find()->where(['DATE(date_created)' => $today])->count();
        $numberfortodayz = ($numberfortoday >= 1) ? $numberfortoday + 1 : 1;
        $customer->queue_id = $steps->queue_id;
        $customer->step_id = $selected;
        $customer->queue = $customer->getQueueNumber($steps->queue_id);
        $customer->status = 1;


        if ($this->request->isPost) {
            if ($customer->load($this->request->post())) {
                
                if ($customer->save()) {
                    $model->customer_id = $customer->id;
                    
                    if ($model->load($this->request->post()) && $model->save()) {
                       
                        return $this->redirect(['queue', 'selected' => $selected, 'id' => $customer->id]);
                    }
                    
                }
            }
        } else {
            $model->loadDefaultValues();
        }
       
        return $this->render('info', [
            'selected' => $selected,
            'infos' => $infos,
            'model' => $model,
            'customer' => $customer,
        ]);
    }

    public function actionQueue($selected, $id)
    {   
        $queue = new Queue();
        $step = new Step();
        $info = new Info();
        $model = new Customer();
        $record = $model->find()->where(['id' => $id])->one();
        
        //$id = Yii::$app->db->getLastInsertID();

        return $this->render('queue', [
            'selected' => $selected,
            'info' => $info,
            'model' => $model,
            'id' => $id,
            'record' => $record,
        ]);
    }
}
