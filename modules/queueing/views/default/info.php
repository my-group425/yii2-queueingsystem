<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Input Information';

//echo $numberfortoday;
//echo $customerid->id;
?>

 <div class="Queueing-default-index text-center ">
  <?php $form = ActiveForm::begin(); ?>
    <h3>Please fill up the following:</h3>
    <div class="flex-container ">
      <?= $form->field($customer, 'queue_id')->hiddenInput()->label(false) ?>
      <?= $form->field($customer, 'step_id')->hiddenInput()->label(false) ?>
      <?= $form->field($customer, 'queue')->hiddenInput()->label(false) ?>
      <?= $form->field($customer, 'status')->hiddenInput()->label(false) ?>

    <?php 

    foreach ($infos as $key => $info) : ?>
        <div class="inputz">
          <?= $form->field($model, 'value')->textInput()->label($info->title) ?>
          <?php //echo $form->field($model, 'customer_id')->textInput(['value' => 1])->label(false) ?>
          <?= $form->field($model, 'info_id')->hiddenInput(['value' => $info->id])->label(false) ?>
              
        </div>
    <?php endforeach; ?>
    </div>
   

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
<?php 

//echo $available;
// foreach ($available as $key => $value) {
//   echo $value;
// }
        // $gfg = new \SplQueue;
  
        // $gfg->push(1);
        // $gfg->push(2);
        // $gfg->push(3);
        

        // echo count($gfg);
       
        // $gfg->shift();
          
       
        //  foreach ($gfg as $elem)  {
        //      echo $elem;
        
        // }

        // $queue = new \SplQueue;
        // $stations = new \SplQueue;
        // $queue = [];
        // $stations = [];
        // $stations->push('s1');
        // $stations->push('s2');
        // $stations->push('s3');

        // $queue->push('c1');      
        // $queue->push('c2');
        // $queue->push('c3');

        // $cycles = count($queue);

        // $count = 0;

        // for ($x = 0; $x < $cycles; $x++) {
        //     $i = $queue->shift();
        //     $stations[$count]->push($i);
            
        //     if ($count >= 3) {
        //         $count = 0;
        //     } else {
        //         $count++;
        //     }
        // }

        // echo $stations[0];
        // echo $stations[1];
        // echo $stations[2];
                

?>

<style>
/*.btn{
    padding: 40px 50px 40px 50px;
    font-size: 30px;
    margin: 10px;
    }
*/
.flex-container {
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  align-items: center;
  justify-content: center;

}

.flex-container >  .inputz {
  
  /*padding: 40px 50px 40px 50px;*/
  margin: 10px;
  /*font-size: 30px;*/
  /*align-items: center;*/

}


</style>



<?php 
$script = <<< JS

function asLi(item) {
 return $('<li>').text(item);
}

function showItem(item) {
 $('ul').append(asLi(item));
}

function showCars() {
 const cars = [];
 showItem('ford');
 fetch(["vw", "bmw", "mercedes"]).then(
   (resp) => {
     showItem('toyota');
     return resp.json();
   }).then((data) => {
     data.forEach(c => showItem(c));
   });
 showItem('honda');
}

document.addEventListener("DOMContentLoaded", function(){
 showCars();
});





JS;
$this->registerJs($script);
?>


