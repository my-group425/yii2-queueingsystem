<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Queue';

//echo $numberfortoday;
?>

<div class="Queueing-default-index text-center ">
  <?php $form = ActiveForm::begin(); ?>
    <h1>How can we help you?</h1>
    <div class="flex-container ">

    <?php foreach ($steps as $key => $stepz) :
      $infos = $info->find()
      ->where(['step_id' => $stepz->id])
      ->all(); 
      $today = date("M d, Y");
     // $recorddate = date('M d, Y',strtotime($record['date_created']));

      echo $form->field($model, 'queue_id')->hiddenInput(['value' => $posted->id])->label(false);
      echo $form->field($model, 'step_id')->hiddenInput(['value' => $stepz->id])->label(false);
      echo $form->field($model, 'status')->hiddenInput(['value' => 1])->label(false);

      switch (count($infos)) {
        case 0:
          echo Html::submitButton($stepz->name, ['class' => 'btn btn-success']);
          break;
      
        default:
          echo Html::a($stepz->name, ['info', 'selected' => $stepz->id], ['class' => 'btn btn-success']);
          break;
      }
      
    ?>
      
      <div class="form-group">
        
      </div>
      
    <?php endforeach; ?>
    </div>
  <?php ActiveForm::end(); ?>
</div>

<style>
/*.btn{
    padding: 40px 50px 40px 50px;
    font-size: 30px;
    margin: 10px;
    }
*/
.flex-container {
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  align-items: center;
  justify-content: center;

}

.flex-container >  .btn {
  
  padding: 40px 50px 40px 50px;
  margin: 10px;
  font-size: 30px;
  /*align-items: center;*/

}


</style>
