<?php 
use yii\helpers\Html;

?>

<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <ul class="nav navbar-nav">
            	<li class="active"><a href="#">QMS</a></li>
            </ul>
            
            <ul class="nav navbar-nav navbar-right">
             	<li><a href="#"><?= date('M d, Y H:m A') ?></a></li>
            </ul>
         </div>
    </nav>
</header>

<div class="container ">
	<div class="text-center">
		<h2>Your number is</h2>
		<h1><?= $record['queue']; ?></h1>

		<h4>Thank you!</h4>
		<h4>Great to serve you!</h4>
		<?= Html::a('Print Queue number', ['index'], ['class' => 'btn btn-primary']) ?>
	</div>

</div>



